What the script does:
- Take the csv file(generated from google response vote form) as a csv file
- Read each line one by one and extract only the info we need: email address, ela address and telegram ID
- Use each email address, ela address and telegram ID and store it as a dictionary so we remove the possibility of having any sort of duplicate entries
- Add the non-duplicate entry to the new dictionary using email hash as the key
- Start writing out the output to a file
- First, use a random seed to be used for random number generator according to what time it is now and then shuffling that further
- Second, select 1 random participant from the non-duplicate dictionary of email addresses, ela addresses and telegram IDs to be the grand prize winner of 200 ELA
- Then, remove this participant from the dictionary so they're not eligible for future prizes
- Use a new random seed based on current time again 
- Select 2 random participants to be the winners of 100 ELAs and then remove these participates from the drawings
- Note: The new seed is chosen after after set of winners for a particular block reward are selected.
- Do the same to select all the winners for 50 ELA(4 winners), 10 ELA(10 winners), 2 ELA(20 winners) and 0.5 ELA(1000 winners)
- Write out the winners list to a csv file which can later be exported into an excel spreadsheet for easy viewing

Example:
- python randomselection.py example.csv output.csv
